import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class AlunoReader {
    
    public static void main(String[] args) {
        List<Aluno> alunos = new ArrayList<>();
        
        // Leitura do arquivo alunos.csv
        try (BufferedReader br = new BufferedReader(new FileReader("alunos.csv"))) {
            String linha;
            while ((linha = br.readLine()) != null) {
                String[] dados = linha.split(",");
                int matricula = Integer.parseInt(dados[0].trim());
                String nome = dados[1].trim();
                double nota = Double.parseDouble(dados[2].trim());
                
                Aluno aluno = new Aluno(matricula, nome, nota);
                alunos.add(aluno);
            }
        } catch (IOException e) {
            System.out.println("Erro ao ler o arquivo alunos.csv: " + e.getMessage());
            return;
        }
        
        // Processamento dos dados
        int quantidadeAlunos = alunos.size();
        int quantidadeAprovados = 0;
        int quantidadeReprovados = 0;
        double menorNota = Double.MAX_VALUE;
        double maiorNota = Double.MIN_VALUE;
        double somaNotas = 0.0;
        
        for (Aluno aluno : alunos) {
            double nota = aluno.getNota();
            if (nota >= 6.0) {
                quantidadeAprovados++;
            } else {
                quantidadeReprovados++;
            }
            
            if (nota < menorNota) {
                menorNota = nota;
            }
            
            if (nota > maiorNota) {
                maiorNota = nota;
            }
            
            somaNotas += nota;
        }
        
        // Cálculo da média
        double mediaGeral = somaNotas / quantidadeAlunos;
        
        // Escrita no arquivo resumo.csv
        try (PrintWriter pw = new PrintWriter(new FileWriter("resumo.csv"))) {
            pw.println("Quantidade de alunos na turma," + quantidadeAlunos);
            pw.println("Quantidade de alunos aprovados," + quantidadeAprovados);
            pw.println("Quantidade de alunos reprovados," + quantidadeReprovados);
            pw.println("Menor nota da turma," + menorNota);
            pw.println("Maior nota da turma," + maiorNota);
            pw.println("Média geral da turma," + mediaGeral);
        } catch (IOException e) {
            System.out.println("Erro ao escrever no arquivo resumo.csv: " + e.getMessage());
            return;
        }
        
        System.out.println("Dados gravados com sucesso no arquivo resumo.csv");
    }
}

